import React, { useState, useEffect } from 'react';

import Container from 'react-bootstrap/Container';


export default function Counter() {
	const [count, setCount] = useState(0);
/*
effect hook - allows to execute a piece of code whenever a component gets rendered to the page or if the value of state changes.
	-useEffect is needed for the page / or a part of, to be reactive
useEffect - has 2 arguments, function & dependency array; BOTH NEEDED
	function - to specify the codes to be executed
	array - to set which variable is to be listened to, in terms of changing the state, for the function to be executed. in some cases, not having any dependency array (ex. [count]) will cause the page to have infinite loops. declaring an empty array would allow useEffect to execute the codes only once.

*/
	useEffect(() =>{
		document.title = `You clicked ${count} times`;
	},[count])

	return(
		<Container fluid>
			<p>You clicked {count} times</p>
			<button onClick={()=> setCount(count + 1)}>Click me!</button>
		</Container>
	)
}