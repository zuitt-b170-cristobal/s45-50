//Import Dependencies
/*
import React from 'react';

import Jumbotron from 'react-bootstrap/Jumbotron';
import Button from 'react-bootstrap/Button';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';


export default function Banner(){
	return(
		<Row>
		 <Col>
		   <Jumbotron>
		     <h1>Zuitt Coding Bootcamp</h1>
		     <p>Opportunities for Everyone, Everywhere.</p>
		     <Button variant="primary">Enroll Now!</Button>
		   </Jumbotron>
		 </Col>
		</Row>
		)
}
*/

// Dependencies
import React from 'react';
import { Row,Col } from 'react-bootstrap';

import { Link } from 'react-router-dom';


export default function Banner( { data } ) {

    console.log(data);
    const {title, content, destination, label} = data;

    return (
        <Row>
            <Col className="p-5">
                <h1>{title}</h1>
                <p>{content}</p>
                <Link to={destination}>{label}</Link>
            </Col>
        </Row>
    )
}
