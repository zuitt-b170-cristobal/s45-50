//dependencies
import React, {useState, useEffect} from 'react';

//bootstrap components
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';


//CourseCard Component
/*
export default function CourseCard(){
	return(
			<Card className="card-course"style={{ width: '18rem' }}>
  			<Card.Body>
    			<Card.Title>Sample Course</Card.Title>
    				<Card.Text>
      				<p>Description:</p> 
      				<p>This is a sample course offering.</p>
      				<p>Price:</p> 
      				<p>Php 40,000</p>
    				</Card.Text>
    				<Button variant="primary">Enroll</Button>
  			</Card.Body>
			</Card>
		)
}
*/
/*
ACTIVITY:

export default function Course(props) {
	let course = props.course;

	const [count, setCount] = useState(0);
	const [seat, setSeat] = useState(10)

	function enroll(){
		if (count < 10){
		setCount (count + 1);
		}else{
			setCount(10)
		}
	}

	function seats(){
		if (seat > 0){
		setSeat (seat - 1);
			}else{
				alert('No more seats left')
			}
	}

	function callBoth(){
		enroll()
		seats()
	}

	return (
        <Card>
            <Card.Body>
                <Card.Title>{course.name}</Card.Title>
                <h6>Description</h6>
                <p>{course.description}</p>
                <h6>Price</h6>
                <p>{course.price}</p>
                <h6>Enrollees</h6>
                <p>{count} Enrollees</p>
                <Button variant="primary" onClick={callBoth}>Enroll</Button>
                </Card.Body>
        </Card>
    )
}

*/
//ACTIVITY ANSWER:

//Props - short term for properties; similar to arguments/ parameters found inside the functions; a way for the parent component to receive info
	//through props, devs can use same component & feed different info/data for rendering


//useState() - a hook used in React to allow components to create manage its own data & is meant to be used internally --accepts an argument that is meant to be the value of 1st element in array
//in React.js, state values must not be changed directly. all changes to the state values must be through setState() funciton
	//-setState() is the 2nd element in the create array

//enrollees will start at 0, the result of useState() is an array of data that is destructured into count& setCount

//setCount() - function to update value of variable depending on times that it is triggered by onClick command (button event)

export default function Course(props) {
	let course = props.course;

	const [ count, setCount ] = useState(0);
	const [isDisabled, setIsDisabled] = useState(0);
	const [seats, setSeats] = useState(30);

	useEffect(()=> {
		if (seats === 0){
			setIsDisabled(true);
		}
	},[seats]);

	 function enroll(){
        setCount(count + 1);
    }

	return (
        <Card>
            <Card.Body>
                <Card.Title>{course.name}</Card.Title>
                <h6>Description</h6>
                <p>{course.description}</p>
                <h6>Price</h6>
                <p>{course.price}</p>
                <h6>Enrollees</h6>
                <p>{count} Enrollees</p>
                <Button variant="primary" onClick={enroll}>Enroll</Button>
                <h6>Seats</h6>
                <p>{seats} remaining</p>
                <Button variant="primary" onClick={()=> setSeats(seats-1)} disabled={isDisabled}>Enroll</Button>
                </Card.Body>
        </Card>
    )
}

