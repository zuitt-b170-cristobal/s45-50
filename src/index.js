//SetUp / Import dependencies
import React, { Fragment } from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter as Router, Route,  Routes} from 'react-router-dom';
import App from './App.js'

/*
ReactDOM.render()
responsible for injecting/inserting the whole React.js Project inside the webpage

'root' - element in index.html; referencing the public folder inside public folder in react-booking
*/

//React.createElement('h1', null, 'Hello World');    --depricated version of render() in react

/*
JSX - JavaScript XML - an extension of JS that let's us create objects w/c wil then be compiled and added as HTML elements

with JSX 
  -we can create HTML elements using JS
  -we can create JS objects that will be compiled & added as HTML elements
*/

/*
Fragment - used to render 2 or more components inside index.js. Without it, webpage will return errors since, it is the of JS to display 2 or more components in the frontend
<>
</>
  -also accepted in place of Fragment but not all browsers are able to read this, also this does not support keys or attributes 
*/
ReactDOM.render(
  <App />
  ,document.getElementById('root')
);