//base imports
import React from 'react';

/*
React.createContext()
	to create a Contect Object
		-a special object that allows info storage w/in the app & pass it around the components

		with the use of context obhect inside react, we'll be able to create global state to store user details instead of getting it from localStorage from time to time

		a different approach to passing info between components w/o use of props & pass it from parent to child since its already being passed on to other components as a global state for the user
*/
export default React.createContext();



