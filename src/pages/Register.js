//Base Imports
import React, {useState, useEffect} from 'react';
import {Navigate} from 'react-router-dom';

import UserContext from '../userContext.js';

//bootstrap dependencies
import {Form, Container, Button} from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function Register(){

	const [user, setUser] = useState({ email: localStorage.getItem('email') });


	const[firstName, setFirstName] = useState('')
	const[lastName, setLastName] = useState('')
	const[age, setAge] = useState('')
	const[gender, setGender] = useState('')
	const[email, setEmail] = useState('')
	const[mobileNo, setMobileNo] = useState('')
	const[password, setPassword] = useState('')
	const[passwordConfirm, setPasswordConfirm] = useState('')
	const[isDisabled, setIsDisabled] = useState(true);

	useEffect(()=>{
		let isFirstNameNotEmpty = firstName !== '';
		let isLastNameNotEmpty = lastName !== '';
		let isAgeNotEmpty = age !== '';
		let isGenderNotEmpty = gender !== '';
		let isEmailNotEmpty = email !== '';
		let isMobileNoNotEmpty = mobileNo !== '';
		let isPasswordNotEmpty = password !== '';
		let isPasswordConfirmNotEmpty = passwordConfirm !== '';
		let isPasswordMatch = password === passwordConfirm;

		if (isFirstNameNotEmpty && isLastNameNotEmpty && isEmailNotEmpty && isMobileNoNotEmpty && isPasswordNotEmpty && isPasswordConfirmNotEmpty && isPasswordMatch) {
			setIsDisabled(false);
		}else{
			setIsDisabled(true);
		}

	},[firstName, lastName, age, gender,email, mobileNo, password, passwordConfirm]);

/*
2 WAY BINDING
	to capture/save input value from the input elements, we can bind the value of the element w/ the states. Devs cannot type into the input fields anymore because there is now value bound to it. Wwe will add an onChange event to be able to update the state that is bound to the input 

	2-way binding is done so that we can assure that we can save the input into our states as the users type into the element. This is so that we don't have to save it before submitting.

e.target.value
e- event to which the element will listen
target- the element where the event will happen
value- the value that the user has entered in that element
*/


	function register(e){
		e.preventDefault();

		fetch('http://localhost:4000/api/users/register', {
        method: 'POST',
        headers: {'Content-Type': 'application/json'},
        body: JSON.stringify({
            firstName: firstName,
            lastName: lastName,
            age: age,
            gender: gender,
            email: email,
            mobileNo: mobileNo,
            password: password,
            passwordConfirm 
        })
    })
    .then(data => data.json())
    .then(data =>  { 
        console.log(data);
        if(data.response !== null){
		Swal.fire('Register successful. You may now log in.');
        } else{
        Swal.fire("Sorry, email has already been taken.");
        } 
    })
    .catch((err) => {
        console.error(err);
	//clears input fields siince they update their respective variable values into an empty string
		setEmail('');
		setPassword('');
		setPasswordConfirm('');
    })

   
}


if (user.email !== null){
		//return <Redirect to ="/" />-redirect is deprecated funciton in react & replaced by navigate
		/*
		Navigate - allows us to redirect users after logging in & updating the global user state. even if the user tries to input the /login as URI, it would still redirect him/her to the homepage.
			REPLACE TO attribute - to specify the page/uri to where the user/s will be redirected
		*/
		return <Navigate replace to ="/" />
	}

	return(
		<Container>
			<Form onSubmit={register}>
				<Form.Group>
					<Form.Label>First Name</Form.Label>
					<Form.Control type="firstName" placeholder="Enter First Name" value={firstName} onChange={(e) => setFirstName(e.target.value)} required />
				</Form.Group>
				<Form.Group>
					<Form.Label>Last Name</Form.Label>
					<Form.Control type="lastName" placeholder="Enter Last Name" value={lastName} onChange={(e) => setLastName(e.target.value)} required />
				</Form.Group>
				<Form.Group>
					<Form.Label>Age</Form.Label>
					<Form.Control type="age" placeholder="Enter Age" value={age} onChange={(e) => setAge(e.target.value)} required />
				</Form.Group>
				<Form.Group>
					<Form.Label>Gender</Form.Label>
					<Form.Control type="gender" placeholder="Enter Gender" value={gender} onChange={(e) => setGender(e.target.value)} required />
				</Form.Group>
				<Form.Group>
					<Form.Label>Email address</Form.Label>
					<Form.Control type="email" placeholder="Enter email" value={email} onChange={(e) => setEmail(e.target.value)} required />
					<Form.Text className='text-muted'>We'll never share your email to anyone else</Form.Text>
				</Form.Group>
				<Form.Group>
					<Form.Label>Mobile Number</Form.Label>
					<Form.Control type="mobileNo" maxLength="11" pattern="[0]{1}[0-9]{10}" placeholder="Enter Mobile Number" value={mobileNo} onChange={(e) => setMobileNo(e.target.value)} required />
				</Form.Group>
				<Form.Group>
					<Form.Label>Password</Form.Label>
					<Form.Control type="password" placeholder="Enter password" value={password} onChange={(e) => setPassword(e.target.value)} required />
				</Form.Group>
			<Form.Group>
					<Form.Label>Confirm Password</Form.Label>
					<Form.Control type="password" placeholder="Confirm password" value={passwordConfirm} onChange={(e) => setPasswordConfirm(e.target.value)} required />
				</Form.Group>
				<Button variant="primary" type="submit" disabled={isDisabled}>Submit</Button>
			</Form>
		</Container>

		)
}