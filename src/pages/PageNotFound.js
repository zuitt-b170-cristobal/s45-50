/*
import React from 'react';
import { Link } from 'react-router-dom';


export default function PageNotFound() {
	return(
	<div>
		<h1>Page Not Found</h1>
		<Link to="/">Go back to homepage</Link>
	</div>
	)
}
*/

import Banner from '../components/Banner';

export default function Error() {

    const data = {
        title: "404 - Not found",
        content: "The page you are looking for cannot be found",
        destination: "/",
        label: "Back home"
    }
    
    return (
        <Banner data={data} />
    )
}

