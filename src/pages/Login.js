//Base Imports
import React, {useState, useEffect, useContext} from 'react';
import {Navigate} from 'react-router-dom';

//App Imports
import UserContext from '../userContext'

//bootstrap dependencies
import {Form, Container, Button} from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function Login(){

	const { user, setUser } = useContext(UserContext);

	const[email, setEmail] = useState('')
	const[password, setPassword] = useState('')
	const[passwordConfirm, setPasswordConfirm] = useState('')
	const[isDisabled, setIsDisabled] = useState(true);

	useEffect(()=>{
		let isEmailNotEmpty = email !== '';
		let isPasswordNotEmpty = password !== '';

		if (isEmailNotEmpty && isPasswordNotEmpty) {
			setIsDisabled(false);
		}else{
			setIsDisabled(true);
		}

	},[email, password]);


	function login(e){
		e.preventDefault();

		fetch('http://localhost:4000/api/users/login', {
			method: "POST",
			headers: {"Content-Type": "application/json"},
			body: JSON.stringify({
				email: email, 
				password
			})
		})
		.then((response) => response.json())
		.then((response) => {
			if (response.access !== undefined){
		Swal.fire('You are now logged in.')

		//localStorage.setItem - to store a piece of data inside the localStorage of browser. this is because the local storage does not delete unless manually done so or the codes make it delete the info inside  
		
				localStorage.setItem('accessToken', response.access);
				setUser({accessToken: response.access})
			}else{
				Swal.fire(response.error);
				setEmail('');
				setPassword('');
			}
		})
	}


			if (user.accessToken !== null){
		//return <Redirect to ="/" />-redirect is deprecated funciton in react & replaced by navigate
		/*
		Navigate - allows us to redirect users after logging in & updating the global user state. even if the user tries to input the /login as URI, it would still redirect him/her to the homepage.
			REPLACE TO attribute - to specify the page/uri to where the user/s will be redirected
		*/
		return <Navigate replace to ="/" />
	}

	return(
		<Container fluid>
			<h3>Login</h3>
			<Form onSubmit={login}>
				<Form.Group>
					<Form.Label>Email address</Form.Label>
					<Form.Control type="email" placeholder="Enter email" value={email} onChange={(e) => setEmail(e.target.value)} required />
					<Form.Text className='text-muted'>We'll never share your email to anyone else</Form.Text>
				</Form.Group>
				<Form.Group>
					<Form.Label>Password</Form.Label>
					<Form.Control type="password" placeholder="Enter password" value={password} onChange={(e) => setPassword(e.target.value)} required />
				</Form.Group>
				<Button variant="primary" type="submit" disabled={isDisabled}>Login</Button>
			</Form>
		</Container>

		)
}