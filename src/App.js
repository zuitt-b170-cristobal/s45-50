//Base Imports
import React, { useState } from 'react';
// import { BrowserRouter, Route, Switch } from 'react-router-dom'; - Switch is depricated that's why we need to use Routes function inside react-router-dom instead
/* 
	react-router-dom allows us to stimulate changing pages in react. Because by default, react is used for SPA - Single Page Application 

	Router - used to wrap components that uses react-router-dom and allows the use of routes and the routing system

	Routes - holds all the Route components (similar to the depricated Switch)

	Route - assigns an endpoint and displays the appropriate page component for that endpoint.
				- path attribute - assigns the endpoint
				-element attribute - assigns the Page component to be displayed at that endpoint (new update)
*/

import {BrowserRouter as Router, Route, Routes} from 'react-router-dom';
import {Navigate} from 'react-router-dom';
//install bootstrap & react bootstrap: bootstrap@4.6.0 react-bootstrap@1.5.
import 'bootstrap/dist/css/bootstrap.min.css';
//css dependency
import './index.css';

//app imports
import UserContext from './userContext.js';

//App Components
//every component has to be imported before it can be rendered inside index.js; universal to all pages if its in all the pages.
import AppNavbar from './components/AppNavbar.js';


//Page Components
import Home from './pages/Home.js';
import Courses from './pages/Courses.js';
import Register from './pages/Register.js';
import Login from './pages/Login.js';
import PageNotFound from './pages/PageNotFound.js';



export default function App(){

//localStorage.getItem - used to retrieve a pc or a whole set of info inside the localStorage. the code below detects if there's a user logged in through use of localStorage.setItem in the Login.js

	const [user, setUser] = useState({ accessToken: localStorage.getItem('accessToken') });

	const unsetUser = () => {
		//localStorage.clear() - to clear every info that is stored inside the localStorage
		localStorage.clear();
		setUser({accessToken: null})
	}



// path '*' - all other unspecified routes, this is to make sure that all other routes, beside the ones in the return statement, will render the Error page

//The PROVIDER component inside useContext is what allows other components to consume or use the context. Any component w/c is not wrapped by the Provider will have access to values provided in the context 
	return(
		<UserContext.Provider value={{user, setUser, unsetUser}}>
			<Router>
    			<AppNavbar user={user} />
    			<Routes>
      				<Route path = "/" element={<Home />} />
      				<Route path = "/courses" element = {<Courses />} />
      				<Route path = "/register" element = {<Register />} />
      				<Route path = "/login" element = {<Login />} />
      				<Route path = "*" element = {<PageNotFound />} />
    			</Routes>
  			</Router>
		</UserContext.Provider>
		)
}