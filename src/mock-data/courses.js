export default [
	{	
		id: "wdc001",
		name: "PHP-LARAVEL",
		description: "Fusce eget erat accumsan, luctus justo sit amet, varius risus. Fusce et mi eu eros accumsan malesuada. Integer vitae auctor ex. In dictum lectus ac nisi imperdiet fermentum. Nulla scelerisque risus quis mi ultricies egestas. Donec urna massa, feugiat ut metus vitae, facilisis molestie mauris. Sed bibendum rhoncus faucibus. Sed viverra quis tortor luctus dignissim. Duis egestas ex vitae lacus egestas, id blandit mi tincidunt. In ut lectus tortor. Suspendisse auctor metus et finibus congue.",
		price: 45000,
		onOffer: true
	},
	{	
		id: "wdc002",
		name: "Python-Django",
		description: "Fusce eget erat accumsan, luctus justo sit amet, varius risus. Fusce et mi eu eros accumsan malesuada. Integer vitae auctor ex. In dictum lectus ac nisi imperdiet fermentum. Nulla scelerisque risus quis mi ultricies egestas. Donec urna massa, feugiat ut metus vitae, facilisis molestie mauris. Sed bibendum rhoncus faucibus. Sed viverra quis tortor luctus dignissim. Duis egestas ex vitae lacus egestas, id blandit mi tincidunt. In ut lectus tortor. Suspendisse auctor metus et finibus congue.",
		price: 50000,
		onOffer: true
	},
	{	
		id: "wdc003",
		name: "Java-Springboot",
		description: "Fusce eget erat accumsan, luctus justo sit amet, varius risus. Fusce et mi eu eros accumsan malesuada. Integer vitae auctor ex. In dictum lectus ac nisi imperdiet fermentum. Nulla scelerisque risus quis mi ultricies egestas. Donec urna massa, feugiat ut metus vitae, facilisis molestie mauris. Sed bibendum rhoncus faucibus. Sed viverra quis tortor luctus dignissim. Duis egestas ex vitae lacus egestas, id blandit mi tincidunt. In ut lectus tortor. Suspendisse auctor metus et finibus congue.",
		price: 55000,
		onOffer: true
	}
]